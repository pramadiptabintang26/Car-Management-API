const { Users } = require("../models");

module.exports = {
  create(createArgs) {
    return Users.create(createArgs);
  },

  update(id, updateArgs) {
    return Users.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return Users.destroy({
      where: {
        id,
      },
    });
  },

  findByPk(id) {
    return Users.findByPk(id);
  },

  findAll() {
    return Users.findAll();
  },

  findOne(id) {
    return Users.findOne(id);
  },

  findTotalUsers() {
    return Users.count();
  },
};
