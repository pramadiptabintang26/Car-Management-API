const carsRepository = require("../repositories/cars");

module.exports = {
  create(requestBody) {
    return carsRepository.create(requestBody);
  },

  update(id, requestBody) {
    return carsRepository.update(id, requestBody);
  },

  delete(id) {
    return carsRepository.delete(id);
  },

  async list(args) {
    try {
      const cars = await carsRepository.findAll(args);
      const countCars = await carsRepository.getTotalCars(args);
      return {
        data: cars,
        count: countCars,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return carsRepository.find(id);
  },

}
