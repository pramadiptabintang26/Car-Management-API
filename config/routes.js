const express = require("express");
const controllers = require("../app/controllers");

const appRouter = express.Router();
const apiRouter = express.Router();

/** Mount GET / handler */
appRouter.get("/", controllers.main.index);

/**
 * TODO: Implement your own API
 *       implementations
 */

//API FOR CARS (endpoint)
//create
apiRouter.post(
  "/api/v1/cars",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isAdminOrSuperAdmin,
  controllers.api.v1.cars.create
);
//update
apiRouter.put(
  "/api/v1/cars/:id",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isAdminOrSuperAdmin,
  controllers.api.v1.cars.update
);
//read data by id
apiRouter.get(
  "/api/v1/cars/:id",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isAdminOrSuperAdmin,
  controllers.api.v1.cars.show
);
//delete
apiRouter.delete(
  "/api/v1/cars/:id",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isAdminOrSuperAdmin,
  controllers.api.v1.cars.destroy
);
//read all data
apiRouter.get("/api/v1/cars", controllers.api.v1.cars.list);


//User API (endpoint)
apiRouter.post("/api/v1/users/register", controllers.api.v1.users.register)
apiRouter.post("/api/v1/users/login", controllers.api.v1.users.login);
apiRouter.get("/api/v1/users/whoami",controllers.api.v1.users.authorize,controllers.api.v1.users.whoAmI);
apiRouter.get("/api/v1/users", controllers.api.v1.users.show);
apiRouter.get("/api/v1/users/:id", controllers.api.v1.users.getUsers);
apiRouter.delete("/api/v1/users/:id",controllers.api.v1.users.destroy)
apiRouter.put(
  "/api/v1/users/:id",
  controllers.api.v1.users.authorize,
  controllers.api.v1.users.isSuperAdmin,
  controllers.api.v1.users.update
);

  
/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
apiRouter.get("/api/v1/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
appRouter.get("/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

appRouter.use(apiRouter);

/** Mount Not Found Handler */
appRouter.use(controllers.main.onLost);

/** Mount Exception Handler */
appRouter.use(controllers.main.onError);

module.exports = appRouter;
